# README #

This project is an application of our Spring class from the IMT (Institute Massachaussette of Technology).

### What is this repository for? ###

* Apply our fresh knowledges about the Spring framework on a concrete project
* 1.2.3

### How do I get set up? ###

* Install a spring environment (you can find one here: https://spring.io/tools)
* RTFM (Read The F***ucking Manual ~2k pages)

### Who do I talk to? ###

* Benjamin Peyresaubes (benjamin.peyresaubes@etudiant.mines-nantes.fr)
* Chuck Norris