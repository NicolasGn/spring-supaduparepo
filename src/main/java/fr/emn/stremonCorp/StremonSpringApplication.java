package fr.emn.stremonCorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StremonSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(StremonSpringApplication.class, args);
	}
}
